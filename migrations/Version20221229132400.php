<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221229132400 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cotizador (id INT AUTO_INCREMENT NOT NULL, moneda VARCHAR(1) NOT NULL, bonus VARCHAR(1) NOT NULL, fecha VARCHAR(255) NOT NULL, adulto VARCHAR(255) NOT NULL, juniors VARCHAR(255) NOT NULL, ninos VARCHAR(255) NOT NULL, menores VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE habitacion (id INT AUTO_INCREMENT NOT NULL, tarifa_fijo_flotante_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, INDEX IDX_F45995BA7140B1D2 (tarifa_fijo_flotante_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hotel (id INT AUTO_INCREMENT NOT NULL, tarifa_fijo_flotante_id INT DEFAULT NULL, optional_ai_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, prefijo_folio VARCHAR(255) NOT NULL, plan VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, descuento_socio VARCHAR(255) NOT NULL, banco VARCHAR(255) NOT NULL, sucursal VARCHAR(255) NOT NULL, beneficiario VARCHAR(255) NOT NULL, numero_cuenta VARCHAR(255) NOT NULL, clabe VARCHAR(255) NOT NULL, adultos VARCHAR(255) NOT NULL, adultos_max VARCHAR(255) NOT NULL, ninos VARCHAR(255) NOT NULL, ninos_max VARCHAR(255) NOT NULL, menores VARCHAR(255) NOT NULL, menores_max VARCHAR(255) NOT NULL, juniors VARCHAR(255) NOT NULL, juniors_max VARCHAR(255) NOT NULL, INDEX IDX_3535ED97140B1D2 (tarifa_fijo_flotante_id), INDEX IDX_3535ED9DF6DEFA1 (optional_ai_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE optional_ai (id INT AUTO_INCREMENT NOT NULL, fecha_ini VARCHAR(255) NOT NULL, fecha_fin VARCHAR(255) NOT NULL, adulto VARCHAR(255) NOT NULL, junior VARCHAR(255) NOT NULL, nino VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tarifa_fijo_flotante (id INT AUTO_INCREMENT NOT NULL, fecha_ini VARCHAR(255) NOT NULL, fecha_fin VARCHAR(255) NOT NULL, tarifa VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(25) NOT NULL, is_active TINYINT(1) NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE habitacion ADD CONSTRAINT FK_F45995BA7140B1D2 FOREIGN KEY (tarifa_fijo_flotante_id) REFERENCES tarifa_fijo_flotante (id)');
        $this->addSql('ALTER TABLE hotel ADD CONSTRAINT FK_3535ED97140B1D2 FOREIGN KEY (tarifa_fijo_flotante_id) REFERENCES tarifa_fijo_flotante (id)');
        $this->addSql('ALTER TABLE hotel ADD CONSTRAINT FK_3535ED9DF6DEFA1 FOREIGN KEY (optional_ai_id) REFERENCES optional_ai (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE habitacion DROP FOREIGN KEY FK_F45995BA7140B1D2');
        $this->addSql('ALTER TABLE hotel DROP FOREIGN KEY FK_3535ED97140B1D2');
        $this->addSql('ALTER TABLE hotel DROP FOREIGN KEY FK_3535ED9DF6DEFA1');
        $this->addSql('DROP TABLE cotizador');
        $this->addSql('DROP TABLE habitacion');
        $this->addSql('DROP TABLE hotel');
        $this->addSql('DROP TABLE optional_ai');
        $this->addSql('DROP TABLE tarifa_fijo_flotante');
        $this->addSql('DROP TABLE user');
    }
}
