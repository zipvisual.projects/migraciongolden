<?php

namespace App\Controller;

use App\Entity\Importaciones;
use App\Form\ImportacionesType;
use App\Repository\ImportacionesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/importaciones")
 */
class ImportacionesController extends AbstractController
{
    /**
     * @Route("/", name="app_importaciones_index", methods={"GET"})
     */
    public function index(ImportacionesRepository $importacionesRepository): Response
    {
        return $this->render('importaciones/index.html.twig', [
            'importaciones' => $importacionesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_importaciones_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ImportacionesRepository $importacionesRepository): Response
    {
        $importacione = new Importaciones();
        $form = $this->createForm(ImportacionesType::class, $importacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $importacionesRepository->add($importacione, true);

            return $this->redirectToRoute('app_importaciones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('importaciones/new.html.twig', [
            'importacione' => $importacione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_importaciones_show", methods={"GET"})
     */
    public function show(Importaciones $importacione): Response
    {
        return $this->render('importaciones/show.html.twig', [
            'importacione' => $importacione,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_importaciones_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Importaciones $importacione, ImportacionesRepository $importacionesRepository): Response
    {
        $form = $this->createForm(ImportacionesType::class, $importacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $importacionesRepository->add($importacione, true);

            return $this->redirectToRoute('app_importaciones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('importaciones/edit.html.twig', [
            'importacione' => $importacione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_importaciones_delete", methods={"POST"})
     */
    public function delete(Request $request, Importaciones $importacione, ImportacionesRepository $importacionesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$importacione->getId(), $request->request->get('_token'))) {
            $importacionesRepository->remove($importacione, true);
        }

        return $this->redirectToRoute('app_importaciones_index', [], Response::HTTP_SEE_OTHER);
    }
}
