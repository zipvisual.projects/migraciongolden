<?php

namespace App\Controller;

use App\Entity\TarifaRCIALLInclusiveMXN;
use App\Form\TarifaRCIALLInclusiveMXNType;
use App\Repository\TarifaRCIALLInclusiveMXNRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarifa/r/c/i/a/l/l/inclusive/m/x/n")
 */
class TarifaRCIALLInclusiveMXNController extends AbstractController
{
    /**
     * @Route("/", name="app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_index", methods={"GET"})
     */
    public function index(TarifaRCIALLInclusiveMXNRepository $tarifaRCIALLInclusiveMXNRepository): Response
    {
        return $this->render('tarifa_rciall_inclusive_mxn/index.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_m_x_ns' => $tarifaRCIALLInclusiveMXNRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TarifaRCIALLInclusiveMXNRepository $tarifaRCIALLInclusiveMXNRepository): Response
    {
        $tarifaRCIALLInclusiveMXN = new TarifaRCIALLInclusiveMXN();
        $form = $this->createForm(TarifaRCIALLInclusiveMXNType::class, $tarifaRCIALLInclusiveMXN);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaRCIALLInclusiveMXNRepository->add($tarifaRCIALLInclusiveMXN, true);

            return $this->redirectToRoute('app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_rciall_inclusive_mxn/new.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_m_x_n' => $tarifaRCIALLInclusiveMXN,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_show", methods={"GET"})
     */
    public function show(TarifaRCIALLInclusiveMXN $tarifaRCIALLInclusiveMXN): Response
    {
        return $this->render('tarifa_rciall_inclusive_mxn/show.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_m_x_n' => $tarifaRCIALLInclusiveMXN,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TarifaRCIALLInclusiveMXN $tarifaRCIALLInclusiveMXN, TarifaRCIALLInclusiveMXNRepository $tarifaRCIALLInclusiveMXNRepository): Response
    {
        $form = $this->createForm(TarifaRCIALLInclusiveMXNType::class, $tarifaRCIALLInclusiveMXN);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaRCIALLInclusiveMXNRepository->add($tarifaRCIALLInclusiveMXN, true);

            return $this->redirectToRoute('app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_rciall_inclusive_mxn/edit.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_m_x_n' => $tarifaRCIALLInclusiveMXN,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_delete", methods={"POST"})
     */
    public function delete(Request $request, TarifaRCIALLInclusiveMXN $tarifaRCIALLInclusiveMXN, TarifaRCIALLInclusiveMXNRepository $tarifaRCIALLInclusiveMXNRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tarifaRCIALLInclusiveMXN->getId(), $request->request->get('_token'))) {
            $tarifaRCIALLInclusiveMXNRepository->remove($tarifaRCIALLInclusiveMXN, true);
        }

        return $this->redirectToRoute('app_tarifa_r_c_i_a_l_l_inclusive_m_x_n_index', [], Response::HTTP_SEE_OTHER);
    }
}
