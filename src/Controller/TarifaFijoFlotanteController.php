<?php

namespace App\Controller;

use App\Entity\TarifaFijoFlotante;
use App\Form\TarifaFijoFlotanteType;
use App\Repository\TarifaFijoFlotanteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarifa_fijoflotante")
 */
class TarifaFijoFlotanteController extends AbstractController
{
    /**
     * @Route("/", name="app_tarifa_fijo_flotante_index", methods={"GET"})
     */
    public function index(TarifaFijoFlotanteRepository $tarifaFijoFlotanteRepository): Response
    {
        return $this->render('tarifa_fijo_flotante/index.html.twig', [
            'tarifa_fijo_flotantes' => $tarifaFijoFlotanteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_tarifa_fijo_flotante_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TarifaFijoFlotanteRepository $tarifaFijoFlotanteRepository): Response
    {
        $tarifaFijoFlotante = new TarifaFijoFlotante();
        $form = $this->createForm(TarifaFijoFlotanteType::class, $tarifaFijoFlotante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaFijoFlotanteRepository->add($tarifaFijoFlotante, true);

            return $this->redirectToRoute('app_tarifa_fijo_flotante_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_fijo_flotante/new.html.twig', [
            'tarifa_fijo_flotante' => $tarifaFijoFlotante,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_fijo_flotante_show", methods={"GET"})
     */
    public function show(TarifaFijoFlotante $tarifaFijoFlotante): Response
    {
        return $this->render('tarifa_fijo_flotante/show.html.twig', [
            'tarifa_fijo_flotante' => $tarifaFijoFlotante,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_tarifa_fijo_flotante_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TarifaFijoFlotante $tarifaFijoFlotante, TarifaFijoFlotanteRepository $tarifaFijoFlotanteRepository): Response
    {
        $form = $this->createForm(TarifaFijoFlotanteType::class, $tarifaFijoFlotante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaFijoFlotanteRepository->add($tarifaFijoFlotante, true);

            return $this->redirectToRoute('app_tarifa_fijo_flotante_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_fijo_flotante/edit.html.twig', [
            'tarifa_fijo_flotante' => $tarifaFijoFlotante,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_fijo_flotante_delete", methods={"POST"})
     */
    public function delete(Request $request, TarifaFijoFlotante $tarifaFijoFlotante, TarifaFijoFlotanteRepository $tarifaFijoFlotanteRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tarifaFijoFlotante->getId(), $request->request->get('_token'))) {
            $tarifaFijoFlotanteRepository->remove($tarifaFijoFlotante, true);
        }

        return $this->redirectToRoute('app_tarifa_fijo_flotante_index', [], Response::HTTP_SEE_OTHER);
    }
}
