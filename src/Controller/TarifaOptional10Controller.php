<?php

namespace App\Controller;

use App\Entity\TarifaOptional10;
use App\Form\TarifaOptional10Type;
use App\Repository\TarifaOptional10Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarifa/optional10")
 */
class TarifaOptional10Controller extends AbstractController
{
    /**
     * @Route("/", name="app_tarifa_optional10_index", methods={"GET"})
     */
    public function index(TarifaOptional10Repository $tarifaOptional10Repository): Response
    {
        return $this->render('tarifa_optional10/index.html.twig', [
            'tarifa_optional10s' => $tarifaOptional10Repository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_tarifa_optional10_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TarifaOptional10Repository $tarifaOptional10Repository): Response
    {
        $tarifaOptional10 = new TarifaOptional10();
        $form = $this->createForm(TarifaOptional10Type::class, $tarifaOptional10);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaOptional10Repository->add($tarifaOptional10, true);

            return $this->redirectToRoute('app_tarifa_optional10_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_optional10/new.html.twig', [
            'tarifa_optional10' => $tarifaOptional10,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_optional10_show", methods={"GET"})
     */
    public function show(TarifaOptional10 $tarifaOptional10): Response
    {
        return $this->render('tarifa_optional10/show.html.twig', [
            'tarifa_optional10' => $tarifaOptional10,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_tarifa_optional10_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TarifaOptional10 $tarifaOptional10, TarifaOptional10Repository $tarifaOptional10Repository): Response
    {
        $form = $this->createForm(TarifaOptional10Type::class, $tarifaOptional10);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaOptional10Repository->add($tarifaOptional10, true);

            return $this->redirectToRoute('app_tarifa_optional10_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_optional10/edit.html.twig', [
            'tarifa_optional10' => $tarifaOptional10,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_optional10_delete", methods={"POST"})
     */
    public function delete(Request $request, TarifaOptional10 $tarifaOptional10, TarifaOptional10Repository $tarifaOptional10Repository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tarifaOptional10->getId(), $request->request->get('_token'))) {
            $tarifaOptional10Repository->remove($tarifaOptional10, true);
        }

        return $this->redirectToRoute('app_tarifa_optional10_index', [], Response::HTTP_SEE_OTHER);
    }
}
