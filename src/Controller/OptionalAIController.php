<?php

namespace App\Controller;

use App\Entity\OptionalAI;
use App\Form\OptionalAIType;
use App\Repository\OptionalAIRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/optional/a/i")
 */
class OptionalAIController extends AbstractController
{
    /**
     * @Route("/", name="app_optional_a_i_index", methods={"GET"})
     */
    public function index(OptionalAIRepository $optionalAIRepository): Response
    {
        return $this->render('optional_ai/index.html.twig', [
            'optional_a_is' => $optionalAIRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_optional_a_i_new", methods={"GET", "POST"})
     */
    public function new(Request $request, OptionalAIRepository $optionalAIRepository): Response
    {
        $optionalAI = new OptionalAI();
        $form = $this->createForm(OptionalAIType::class, $optionalAI);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $optionalAIRepository->add($optionalAI, true);

            return $this->redirectToRoute('app_optional_a_i_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('optional_ai/new.html.twig', [
            'optional_a_i' => $optionalAI,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_optional_a_i_show", methods={"GET"})
     */
    public function show(OptionalAI $optionalAI): Response
    {
        return $this->render('optional_ai/show.html.twig', [
            'optional_a_i' => $optionalAI,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_optional_a_i_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, OptionalAI $optionalAI, OptionalAIRepository $optionalAIRepository): Response
    {
        $form = $this->createForm(OptionalAIType::class, $optionalAI);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $optionalAIRepository->add($optionalAI, true);

            return $this->redirectToRoute('app_optional_a_i_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('optional_ai/edit.html.twig', [
            'optional_a_i' => $optionalAI,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_optional_a_i_delete", methods={"POST"})
     */
    public function delete(Request $request, OptionalAI $optionalAI, OptionalAIRepository $optionalAIRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$optionalAI->getId(), $request->request->get('_token'))) {
            $optionalAIRepository->remove($optionalAI, true);
        }

        return $this->redirectToRoute('app_optional_a_i_index', [], Response::HTTP_SEE_OTHER);
    }
}
