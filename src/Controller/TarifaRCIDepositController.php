<?php

namespace App\Controller;

use App\Entity\TarifaRCIDeposit;
use App\Form\TarifaRCIDepositType;
use App\Repository\TarifaRCIDepositRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarifa/r/c/i/deposit")
 */
class TarifaRCIDepositController extends AbstractController
{
    /**
     * @Route("/", name="app_tarifa_r_c_i_deposit_index", methods={"GET"})
     */
    public function index(TarifaRCIDepositRepository $tarifaRCIDepositRepository): Response
    {
        return $this->render('tarifa_rci_deposit/index.html.twig', [
            'tarifa_r_c_i_deposits' => $tarifaRCIDepositRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_tarifa_r_c_i_deposit_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TarifaRCIDepositRepository $tarifaRCIDepositRepository): Response
    {
        $tarifaRCIDeposit = new TarifaRCIDeposit();
        $form = $this->createForm(TarifaRCIDepositType::class, $tarifaRCIDeposit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaRCIDepositRepository->add($tarifaRCIDeposit, true);

            return $this->redirectToRoute('app_tarifa_r_c_i_deposit_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_rci_deposit/new.html.twig', [
            'tarifa_r_c_i_deposit' => $tarifaRCIDeposit,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_r_c_i_deposit_show", methods={"GET"})
     */
    public function show(TarifaRCIDeposit $tarifaRCIDeposit): Response
    {
        return $this->render('tarifa_rci_deposit/show.html.twig', [
            'tarifa_r_c_i_deposit' => $tarifaRCIDeposit,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_tarifa_r_c_i_deposit_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TarifaRCIDeposit $tarifaRCIDeposit, TarifaRCIDepositRepository $tarifaRCIDepositRepository): Response
    {
        $form = $this->createForm(TarifaRCIDepositType::class, $tarifaRCIDeposit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaRCIDepositRepository->add($tarifaRCIDeposit, true);

            return $this->redirectToRoute('app_tarifa_r_c_i_deposit_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_rci_deposit/edit.html.twig', [
            'tarifa_r_c_i_deposit' => $tarifaRCIDeposit,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_r_c_i_deposit_delete", methods={"POST"})
     */
    public function delete(Request $request, TarifaRCIDeposit $tarifaRCIDeposit, TarifaRCIDepositRepository $tarifaRCIDepositRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tarifaRCIDeposit->getId(), $request->request->get('_token'))) {
            $tarifaRCIDepositRepository->remove($tarifaRCIDeposit, true);
        }

        return $this->redirectToRoute('app_tarifa_r_c_i_deposit_index', [], Response::HTTP_SEE_OTHER);
    }
}
