<?php

namespace App\Controller;

use App\Entity\Cierres;
use App\Form\CierresType;
use App\Repository\CierresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cierres")
 */
class CierresController extends AbstractController
{
    /**
     * @Route("/", name="app_cierres_index", methods={"GET"})
     */
    public function index(CierresRepository $cierresRepository): Response
    {
        return $this->render('cierres/index.html.twig', [
            'cierres' => $cierresRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_cierres_new", methods={"GET", "POST"})
     */
    public function new(Request $request, CierresRepository $cierresRepository): Response
    {
        $cierre = new Cierres();
        $form = $this->createForm(CierresType::class, $cierre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cierresRepository->add($cierre, true);

            return $this->redirectToRoute('app_cierres_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cierres/new.html.twig', [
            'cierre' => $cierre,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_cierres_show", methods={"GET"})
     */
    public function show(Cierres $cierre): Response
    {
        return $this->render('cierres/show.html.twig', [
            'cierre' => $cierre,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_cierres_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Cierres $cierre, CierresRepository $cierresRepository): Response
    {
        $form = $this->createForm(CierresType::class, $cierre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cierresRepository->add($cierre, true);

            return $this->redirectToRoute('app_cierres_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cierres/edit.html.twig', [
            'cierre' => $cierre,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_cierres_delete", methods={"POST"})
     */
    public function delete(Request $request, Cierres $cierre, CierresRepository $cierresRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cierre->getId(), $request->request->get('_token'))) {
            $cierresRepository->remove($cierre, true);
        }

        return $this->redirectToRoute('app_cierres_index', [], Response::HTTP_SEE_OTHER);
    }
}
