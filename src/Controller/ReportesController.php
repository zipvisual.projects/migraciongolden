<?php

namespace App\Controller;

use App\Entity\Reportes;
use App\Form\ReportesType;
use App\Repository\ReportesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reportes")
 */
class ReportesController extends AbstractController
{
    /**
     * @Route("/", name="app_reportes_index", methods={"GET"})
     */
    public function index(ReportesRepository $reportesRepository): Response
    {
        return $this->render('reportes/index.html.twig', [
            'reportes' => $reportesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_reportes_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ReportesRepository $reportesRepository): Response
    {
        $reporte = new Reportes();
        $form = $this->createForm(ReportesType::class, $reporte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reportesRepository->add($reporte, true);

            return $this->redirectToRoute('app_reportes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('reportes/new.html.twig', [
            'reporte' => $reporte,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_reportes_show", methods={"GET"})
     */
    public function show(Reportes $reporte): Response
    {
        return $this->render('reportes/show.html.twig', [
            'reporte' => $reporte,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_reportes_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Reportes $reporte, ReportesRepository $reportesRepository): Response
    {
        $form = $this->createForm(ReportesType::class, $reporte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reportesRepository->add($reporte, true);

            return $this->redirectToRoute('app_reportes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('reportes/edit.html.twig', [
            'reporte' => $reporte,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_reportes_delete", methods={"POST"})
     */
    public function delete(Request $request, Reportes $reporte, ReportesRepository $reportesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reporte->getId(), $request->request->get('_token'))) {
            $reportesRepository->remove($reporte, true);
        }

        return $this->redirectToRoute('app_reportes_index', [], Response::HTTP_SEE_OTHER);
    }
}
