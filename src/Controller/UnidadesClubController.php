<?php

namespace App\Controller;

use App\Entity\UnidadesClub;
use App\Form\UnidadesClubType;
use App\Repository\UnidadesClubRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/unidades/club")
 */
class UnidadesClubController extends AbstractController
{
    /**
     * @Route("/", name="app_unidades_club_index", methods={"GET"})
     */
    public function index(UnidadesClubRepository $unidadesClubRepository): Response
    {
        return $this->render('unidades_club/index.html.twig', [
            'unidades_clubs' => $unidadesClubRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_unidades_club_new", methods={"GET", "POST"})
     */
    public function new(Request $request, UnidadesClubRepository $unidadesClubRepository): Response
    {
        $unidadesClub = new UnidadesClub();
        $form = $this->createForm(UnidadesClubType::class, $unidadesClub);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $unidadesClubRepository->add($unidadesClub, true);

            return $this->redirectToRoute('app_unidades_club_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('unidades_club/new.html.twig', [
            'unidades_club' => $unidadesClub,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_unidades_club_show", methods={"GET"})
     */
    public function show(UnidadesClub $unidadesClub): Response
    {
        return $this->render('unidades_club/show.html.twig', [
            'unidades_club' => $unidadesClub,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_unidades_club_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, UnidadesClub $unidadesClub, UnidadesClubRepository $unidadesClubRepository): Response
    {
        $form = $this->createForm(UnidadesClubType::class, $unidadesClub);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $unidadesClubRepository->add($unidadesClub, true);

            return $this->redirectToRoute('app_unidades_club_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('unidades_club/edit.html.twig', [
            'unidades_club' => $unidadesClub,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_unidades_club_delete", methods={"POST"})
     */
    public function delete(Request $request, UnidadesClub $unidadesClub, UnidadesClubRepository $unidadesClubRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$unidadesClub->getId(), $request->request->get('_token'))) {
            $unidadesClubRepository->remove($unidadesClub, true);
        }

        return $this->redirectToRoute('app_unidades_club_index', [], Response::HTTP_SEE_OTHER);
    }
}
