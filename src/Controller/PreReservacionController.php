<?php

namespace App\Controller;

use App\Entity\PreReservacion;
use App\Form\PreReservacionType;
use App\Repository\PreReservacionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pre/reservacion")
 */
class PreReservacionController extends AbstractController
{
    /**
     * @Route("/", name="app_pre_reservacion_index", methods={"GET"})
     */
    public function index(PreReservacionRepository $preReservacionRepository): Response
    {
        return $this->render('pre_reservacion/index.html.twig', [
            'pre_reservacions' => $preReservacionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_pre_reservacion_new", methods={"GET", "POST"})
     */
    public function new(Request $request, PreReservacionRepository $preReservacionRepository): Response
    {
        $preReservacion = new PreReservacion();
        $form = $this->createForm(PreReservacionType::class, $preReservacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $preReservacionRepository->add($preReservacion, true);

            return $this->redirectToRoute('app_pre_reservacion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pre_reservacion/new.html.twig', [
            'pre_reservacion' => $preReservacion,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_pre_reservacion_show", methods={"GET"})
     */
    public function show(PreReservacion $preReservacion): Response
    {
        return $this->render('pre_reservacion/show.html.twig', [
            'pre_reservacion' => $preReservacion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_pre_reservacion_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, PreReservacion $preReservacion, PreReservacionRepository $preReservacionRepository): Response
    {
        $form = $this->createForm(PreReservacionType::class, $preReservacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $preReservacionRepository->add($preReservacion, true);

            return $this->redirectToRoute('app_pre_reservacion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pre_reservacion/edit.html.twig', [
            'pre_reservacion' => $preReservacion,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_pre_reservacion_delete", methods={"POST"})
     */
    public function delete(Request $request, PreReservacion $preReservacion, PreReservacionRepository $preReservacionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$preReservacion->getId(), $request->request->get('_token'))) {
            $preReservacionRepository->remove($preReservacion, true);
        }

        return $this->redirectToRoute('app_pre_reservacion_index', [], Response::HTTP_SEE_OTHER);
    }
}
