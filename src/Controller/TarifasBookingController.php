<?php

namespace App\Controller;

use App\Entity\TarifasBooking;
use App\Form\TarifasBookingType;
use App\Repository\TarifasBookingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarifas/booking")
 */
class TarifasBookingController extends AbstractController
{
    /**
     * @Route("/", name="app_tarifas_booking_index", methods={"GET"})
     */
    public function index(TarifasBookingRepository $tarifasBookingRepository): Response
    {
        return $this->render('tarifas_booking/index.html.twig', [
            'tarifas_bookings' => $tarifasBookingRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_tarifas_booking_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TarifasBookingRepository $tarifasBookingRepository): Response
    {
        $tarifasBooking = new TarifasBooking();
        $form = $this->createForm(TarifasBookingType::class, $tarifasBooking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifasBookingRepository->add($tarifasBooking, true);

            return $this->redirectToRoute('app_tarifas_booking_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifas_booking/new.html.twig', [
            'tarifas_booking' => $tarifasBooking,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifas_booking_show", methods={"GET"})
     */
    public function show(TarifasBooking $tarifasBooking): Response
    {
        return $this->render('tarifas_booking/show.html.twig', [
            'tarifas_booking' => $tarifasBooking,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_tarifas_booking_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TarifasBooking $tarifasBooking, TarifasBookingRepository $tarifasBookingRepository): Response
    {
        $form = $this->createForm(TarifasBookingType::class, $tarifasBooking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifasBookingRepository->add($tarifasBooking, true);

            return $this->redirectToRoute('app_tarifas_booking_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifas_booking/edit.html.twig', [
            'tarifas_booking' => $tarifasBooking,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifas_booking_delete", methods={"POST"})
     */
    public function delete(Request $request, TarifasBooking $tarifasBooking, TarifasBookingRepository $tarifasBookingRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tarifasBooking->getId(), $request->request->get('_token'))) {
            $tarifasBookingRepository->remove($tarifasBooking, true);
        }

        return $this->redirectToRoute('app_tarifas_booking_index', [], Response::HTTP_SEE_OTHER);
    }
}
