<?php

namespace App\Controller;

use App\Entity\CierreHabitaciones;
use App\Form\CierreHabitacionesType;
use App\Repository\CierreHabitacionesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cierre/habitaciones")
 */
class CierreHabitacionesController extends AbstractController
{
    /**
     * @Route("/", name="app_cierre_habitaciones_index", methods={"GET"})
     */
    public function index(CierreHabitacionesRepository $cierreHabitacionesRepository): Response
    {
        return $this->render('cierre_habitaciones/index.html.twig', [
            'cierre_habitaciones' => $cierreHabitacionesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_cierre_habitaciones_new", methods={"GET", "POST"})
     */
    public function new(Request $request, CierreHabitacionesRepository $cierreHabitacionesRepository): Response
    {
        $cierreHabitacione = new CierreHabitaciones();
        $form = $this->createForm(CierreHabitacionesType::class, $cierreHabitacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cierreHabitacionesRepository->add($cierreHabitacione, true);

            return $this->redirectToRoute('app_cierre_habitaciones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cierre_habitaciones/new.html.twig', [
            'cierre_habitacione' => $cierreHabitacione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_cierre_habitaciones_show", methods={"GET"})
     */
    public function show(CierreHabitaciones $cierreHabitacione): Response
    {
        return $this->render('cierre_habitaciones/show.html.twig', [
            'cierre_habitacione' => $cierreHabitacione,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_cierre_habitaciones_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, CierreHabitaciones $cierreHabitacione, CierreHabitacionesRepository $cierreHabitacionesRepository): Response
    {
        $form = $this->createForm(CierreHabitacionesType::class, $cierreHabitacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cierreHabitacionesRepository->add($cierreHabitacione, true);

            return $this->redirectToRoute('app_cierre_habitaciones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cierre_habitaciones/edit.html.twig', [
            'cierre_habitacione' => $cierreHabitacione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_cierre_habitaciones_delete", methods={"POST"})
     */
    public function delete(Request $request, CierreHabitaciones $cierreHabitacione, CierreHabitacionesRepository $cierreHabitacionesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cierreHabitacione->getId(), $request->request->get('_token'))) {
            $cierreHabitacionesRepository->remove($cierreHabitacione, true);
        }

        return $this->redirectToRoute('app_cierre_habitaciones_index', [], Response::HTTP_SEE_OTHER);
    }
}
