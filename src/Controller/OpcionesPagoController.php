<?php

namespace App\Controller;

use App\Entity\OpcionesPago;
use App\Form\OpcionesPagoType;
use App\Repository\OpcionesPagoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/opciones/pago")
 */
class OpcionesPagoController extends AbstractController
{
    /**
     * @Route("/", name="app_opciones_pago_index", methods={"GET"})
     */
    public function index(OpcionesPagoRepository $opcionesPagoRepository): Response
    {
        return $this->render('opciones_pago/index.html.twig', [
            'opciones_pagos' => $opcionesPagoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_opciones_pago_new", methods={"GET", "POST"})
     */
    public function new(Request $request, OpcionesPagoRepository $opcionesPagoRepository): Response
    {
        $opcionesPago = new OpcionesPago();
        $form = $this->createForm(OpcionesPagoType::class, $opcionesPago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $opcionesPagoRepository->add($opcionesPago, true);

            return $this->redirectToRoute('app_opciones_pago_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('opciones_pago/new.html.twig', [
            'opciones_pago' => $opcionesPago,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_opciones_pago_show", methods={"GET"})
     */
    public function show(OpcionesPago $opcionesPago): Response
    {
        return $this->render('opciones_pago/show.html.twig', [
            'opciones_pago' => $opcionesPago,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_opciones_pago_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, OpcionesPago $opcionesPago, OpcionesPagoRepository $opcionesPagoRepository): Response
    {
        $form = $this->createForm(OpcionesPagoType::class, $opcionesPago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $opcionesPagoRepository->add($opcionesPago, true);

            return $this->redirectToRoute('app_opciones_pago_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('opciones_pago/edit.html.twig', [
            'opciones_pago' => $opcionesPago,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_opciones_pago_delete", methods={"POST"})
     */
    public function delete(Request $request, OpcionesPago $opcionesPago, OpcionesPagoRepository $opcionesPagoRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$opcionesPago->getId(), $request->request->get('_token'))) {
            $opcionesPagoRepository->remove($opcionesPago, true);
        }

        return $this->redirectToRoute('app_opciones_pago_index', [], Response::HTTP_SEE_OTHER);
    }
}
