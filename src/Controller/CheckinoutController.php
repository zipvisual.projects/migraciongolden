<?php

namespace App\Controller;

use App\Entity\Checkinout;
use App\Form\CheckinoutType;
use App\Repository\CheckinoutRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/checkinout")
 */
class CheckinoutController extends AbstractController
{
    /**
     * @Route("/", name="app_checkinout_index", methods={"GET"})
     */
    public function index(CheckinoutRepository $checkinoutRepository): Response
    {
        return $this->render('checkinout/index.html.twig', [
            'checkinouts' => $checkinoutRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_checkinout_new", methods={"GET", "POST"})
     */
    public function new(Request $request, CheckinoutRepository $checkinoutRepository): Response
    {
        $checkinout = new Checkinout();
        $form = $this->createForm(CheckinoutType::class, $checkinout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $checkinoutRepository->add($checkinout, true);

            return $this->redirectToRoute('app_checkinout_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('checkinout/new.html.twig', [
            'checkinout' => $checkinout,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_checkinout_show", methods={"GET"})
     */
    public function show(Checkinout $checkinout): Response
    {
        return $this->render('checkinout/show.html.twig', [
            'checkinout' => $checkinout,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_checkinout_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Checkinout $checkinout, CheckinoutRepository $checkinoutRepository): Response
    {
        $form = $this->createForm(CheckinoutType::class, $checkinout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $checkinoutRepository->add($checkinout, true);

            return $this->redirectToRoute('app_checkinout_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('checkinout/edit.html.twig', [
            'checkinout' => $checkinout,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_checkinout_delete", methods={"POST"})
     */
    public function delete(Request $request, Checkinout $checkinout, CheckinoutRepository $checkinoutRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$checkinout->getId(), $request->request->get('_token'))) {
            $checkinoutRepository->remove($checkinout, true);
        }

        return $this->redirectToRoute('app_checkinout_index', [], Response::HTTP_SEE_OTHER);
    }
}
