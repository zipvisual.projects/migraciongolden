<?php

namespace App\Controller;

use App\Entity\Promociones;
use App\Form\PromocionesType;
use App\Repository\PromocionesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/promociones")
 */
class PromocionesController extends AbstractController
{
    /**
     * @Route("/", name="app_promociones_index", methods={"GET"})
     */
    public function index(PromocionesRepository $promocionesRepository): Response
    {
        return $this->render('promociones/index.html.twig', [
            'promociones' => $promocionesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_promociones_new", methods={"GET", "POST"})
     */
    public function new(Request $request, PromocionesRepository $promocionesRepository): Response
    {
        $promocione = new Promociones();
        $form = $this->createForm(PromocionesType::class, $promocione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promocionesRepository->add($promocione, true);

            return $this->redirectToRoute('app_promociones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('promociones/new.html.twig', [
            'promocione' => $promocione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_promociones_show", methods={"GET"})
     */
    public function show(Promociones $promocione): Response
    {
        return $this->render('promociones/show.html.twig', [
            'promocione' => $promocione,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_promociones_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Promociones $promocione, PromocionesRepository $promocionesRepository): Response
    {
        $form = $this->createForm(PromocionesType::class, $promocione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promocionesRepository->add($promocione, true);

            return $this->redirectToRoute('app_promociones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('promociones/edit.html.twig', [
            'promocione' => $promocione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_promociones_delete", methods={"POST"})
     */
    public function delete(Request $request, Promociones $promocione, PromocionesRepository $promocionesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$promocione->getId(), $request->request->get('_token'))) {
            $promocionesRepository->remove($promocione, true);
        }

        return $this->redirectToRoute('app_promociones_index', [], Response::HTTP_SEE_OTHER);
    }
}
