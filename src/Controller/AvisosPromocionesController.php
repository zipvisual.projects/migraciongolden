<?php

namespace App\Controller;

use App\Entity\AvisosPromociones;
use App\Form\AvisosPromocionesType;
use App\Repository\AvisosPromocionesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/avisos/promociones")
 */
class AvisosPromocionesController extends AbstractController
{
    /**
     * @Route("/", name="app_avisos_promociones_index", methods={"GET"})
     */
    public function index(AvisosPromocionesRepository $avisosPromocionesRepository): Response
    {
        return $this->render('avisos_promociones/index.html.twig', [
            'avisos_promociones' => $avisosPromocionesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_avisos_promociones_new", methods={"GET", "POST"})
     */
    public function new(Request $request, AvisosPromocionesRepository $avisosPromocionesRepository): Response
    {
        $avisosPromocione = new AvisosPromociones();
        $form = $this->createForm(AvisosPromocionesType::class, $avisosPromocione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $avisosPromocionesRepository->add($avisosPromocione, true);

            return $this->redirectToRoute('app_avisos_promociones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('avisos_promociones/new.html.twig', [
            'avisos_promocione' => $avisosPromocione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_avisos_promociones_show", methods={"GET"})
     */
    public function show(AvisosPromociones $avisosPromocione): Response
    {
        return $this->render('avisos_promociones/show.html.twig', [
            'avisos_promocione' => $avisosPromocione,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_avisos_promociones_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, AvisosPromociones $avisosPromocione, AvisosPromocionesRepository $avisosPromocionesRepository): Response
    {
        $form = $this->createForm(AvisosPromocionesType::class, $avisosPromocione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $avisosPromocionesRepository->add($avisosPromocione, true);

            return $this->redirectToRoute('app_avisos_promociones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('avisos_promociones/edit.html.twig', [
            'avisos_promocione' => $avisosPromocione,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_avisos_promociones_delete", methods={"POST"})
     */
    public function delete(Request $request, AvisosPromociones $avisosPromocione, AvisosPromocionesRepository $avisosPromocionesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$avisosPromocione->getId(), $request->request->get('_token'))) {
            $avisosPromocionesRepository->remove($avisosPromocione, true);
        }

        return $this->redirectToRoute('app_avisos_promociones_index', [], Response::HTTP_SEE_OTHER);
    }
}
