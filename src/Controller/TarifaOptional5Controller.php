<?php

namespace App\Controller;

use App\Entity\TarifaOptional5;
use App\Form\TarifaOptional5Type;
use App\Repository\TarifaOptional5Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarifa/optional5")
 */
class TarifaOptional5Controller extends AbstractController
{
    /**
     * @Route("/", name="app_tarifa_optional5_index", methods={"GET"})
     */
    public function index(TarifaOptional5Repository $tarifaOptional5Repository): Response
    {
        return $this->render('tarifa_optional5/index.html.twig', [
            'tarifa_optional5s' => $tarifaOptional5Repository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_tarifa_optional5_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TarifaOptional5Repository $tarifaOptional5Repository): Response
    {
        $tarifaOptional5 = new TarifaOptional5();
        $form = $this->createForm(TarifaOptional5Type::class, $tarifaOptional5);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaOptional5Repository->add($tarifaOptional5, true);

            return $this->redirectToRoute('app_tarifa_optional5_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_optional5/new.html.twig', [
            'tarifa_optional5' => $tarifaOptional5,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_optional5_show", methods={"GET"})
     */
    public function show(TarifaOptional5 $tarifaOptional5): Response
    {
        return $this->render('tarifa_optional5/show.html.twig', [
            'tarifa_optional5' => $tarifaOptional5,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_tarifa_optional5_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TarifaOptional5 $tarifaOptional5, TarifaOptional5Repository $tarifaOptional5Repository): Response
    {
        $form = $this->createForm(TarifaOptional5Type::class, $tarifaOptional5);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaOptional5Repository->add($tarifaOptional5, true);

            return $this->redirectToRoute('app_tarifa_optional5_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_optional5/edit.html.twig', [
            'tarifa_optional5' => $tarifaOptional5,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_optional5_delete", methods={"POST"})
     */
    public function delete(Request $request, TarifaOptional5 $tarifaOptional5, TarifaOptional5Repository $tarifaOptional5Repository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tarifaOptional5->getId(), $request->request->get('_token'))) {
            $tarifaOptional5Repository->remove($tarifaOptional5, true);
        }

        return $this->redirectToRoute('app_tarifa_optional5_index', [], Response::HTTP_SEE_OTHER);
    }
}
