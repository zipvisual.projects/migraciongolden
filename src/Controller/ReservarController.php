<?php

namespace App\Controller;

use App\Entity\Reservar;
use App\Form\ReservarType;
use App\Repository\ReservarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reservar")
 */
class ReservarController extends AbstractController
{
    /**
     * @Route("/", name="app_reservar_index", methods={"GET"})
     */
    public function index(ReservarRepository $reservarRepository): Response
    {
        return $this->render('reservar/index.html.twig', [
            'reservars' => $reservarRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_reservar_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ReservarRepository $reservarRepository): Response
    {
        $reservar = new Reservar();
        $form = $this->createForm(ReservarType::class, $reservar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reservarRepository->add($reservar, true);

            return $this->redirectToRoute('app_reservar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('reservar/new.html.twig', [
            'reservar' => $reservar,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_reservar_show", methods={"GET"})
     */
    public function show(Reservar $reservar): Response
    {
        return $this->render('reservar/show.html.twig', [
            'reservar' => $reservar,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_reservar_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Reservar $reservar, ReservarRepository $reservarRepository): Response
    {
        $form = $this->createForm(ReservarType::class, $reservar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reservarRepository->add($reservar, true);

            return $this->redirectToRoute('app_reservar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('reservar/edit.html.twig', [
            'reservar' => $reservar,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_reservar_delete", methods={"POST"})
     */
    public function delete(Request $request, Reservar $reservar, ReservarRepository $reservarRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reservar->getId(), $request->request->get('_token'))) {
            $reservarRepository->remove($reservar, true);
        }

        return $this->redirectToRoute('app_reservar_index', [], Response::HTTP_SEE_OTHER);
    }
}
