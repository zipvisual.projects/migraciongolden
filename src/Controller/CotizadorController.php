<?php

namespace App\Controller;

use App\Entity\Cotizador;
use App\Form\CotizadorType;
use App\Repository\CotizadorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cotizador")
 */
class CotizadorController extends AbstractController
{
    /**
     * @Route("/", name="app_cotizador_index", methods={"GET"})
     */
    public function index(CotizadorRepository $cotizadorRepository): Response
    {
        return $this->render('cotizador/index.html.twig', [
            'cotizadors' => $cotizadorRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_cotizador_new", methods={"GET", "POST"})
     */
    public function new(Request $request, CotizadorRepository $cotizadorRepository): Response
    {
        $cotizador = new Cotizador();
        $form = $this->createForm(CotizadorType::class, $cotizador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cotizadorRepository->add($cotizador, true);

            return $this->redirectToRoute('app_cotizador_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cotizador/new.html.twig', [
            'cotizador' => $cotizador,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_cotizador_show", methods={"GET"})
     */
    public function show(Cotizador $cotizador): Response
    {
        return $this->render('cotizador/show.html.twig', [
            'cotizador' => $cotizador,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_cotizador_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Cotizador $cotizador, CotizadorRepository $cotizadorRepository): Response
    {
        $form = $this->createForm(CotizadorType::class, $cotizador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cotizadorRepository->add($cotizador, true);

            return $this->redirectToRoute('app_cotizador_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cotizador/edit.html.twig', [
            'cotizador' => $cotizador,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_cotizador_delete", methods={"POST"})
     */
    public function delete(Request $request, Cotizador $cotizador, CotizadorRepository $cotizadorRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cotizador->getId(), $request->request->get('_token'))) {
            $cotizadorRepository->remove($cotizador, true);
        }

        return $this->redirectToRoute('app_cotizador_index', [], Response::HTTP_SEE_OTHER);
    }
}
