<?php

namespace App\Controller;

use App\Entity\TarifaRCIALLInclusiveUSD;
use App\Form\TarifaRCIALLInclusiveUSDType;
use App\Repository\TarifaRCIALLInclusiveUSDRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tarifa/r/c/i/a/l/l/inclusive/u/s/d")
 */
class TarifaRCIALLInclusiveUSDController extends AbstractController
{
    /**
     * @Route("/", name="app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_index", methods={"GET"})
     */
    public function index(TarifaRCIALLInclusiveUSDRepository $tarifaRCIALLInclusiveUSDRepository): Response
    {
        return $this->render('tarifa_rciall_inclusive_usd/index.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_u_s_ds' => $tarifaRCIALLInclusiveUSDRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TarifaRCIALLInclusiveUSDRepository $tarifaRCIALLInclusiveUSDRepository): Response
    {
        $tarifaRCIALLInclusiveUSD = new TarifaRCIALLInclusiveUSD();
        $form = $this->createForm(TarifaRCIALLInclusiveUSDType::class, $tarifaRCIALLInclusiveUSD);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaRCIALLInclusiveUSDRepository->add($tarifaRCIALLInclusiveUSD, true);

            return $this->redirectToRoute('app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_rciall_inclusive_usd/new.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_u_s_d' => $tarifaRCIALLInclusiveUSD,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_show", methods={"GET"})
     */
    public function show(TarifaRCIALLInclusiveUSD $tarifaRCIALLInclusiveUSD): Response
    {
        return $this->render('tarifa_rciall_inclusive_usd/show.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_u_s_d' => $tarifaRCIALLInclusiveUSD,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TarifaRCIALLInclusiveUSD $tarifaRCIALLInclusiveUSD, TarifaRCIALLInclusiveUSDRepository $tarifaRCIALLInclusiveUSDRepository): Response
    {
        $form = $this->createForm(TarifaRCIALLInclusiveUSDType::class, $tarifaRCIALLInclusiveUSD);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tarifaRCIALLInclusiveUSDRepository->add($tarifaRCIALLInclusiveUSD, true);

            return $this->redirectToRoute('app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tarifa_rciall_inclusive_usd/edit.html.twig', [
            'tarifa_r_c_i_a_l_l_inclusive_u_s_d' => $tarifaRCIALLInclusiveUSD,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_delete", methods={"POST"})
     */
    public function delete(Request $request, TarifaRCIALLInclusiveUSD $tarifaRCIALLInclusiveUSD, TarifaRCIALLInclusiveUSDRepository $tarifaRCIALLInclusiveUSDRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tarifaRCIALLInclusiveUSD->getId(), $request->request->get('_token'))) {
            $tarifaRCIALLInclusiveUSDRepository->remove($tarifaRCIALLInclusiveUSD, true);
        }

        return $this->redirectToRoute('app_tarifa_r_c_i_a_l_l_inclusive_u_s_d_index', [], Response::HTTP_SEE_OTHER);
    }
}
