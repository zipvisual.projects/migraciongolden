<?php

namespace App\Controller;

use App\Entity\EstanciaMinima;
use App\Form\EstanciaMinimaType;
use App\Repository\EstanciaMinimaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/estancia/minima")
 */
class EstanciaMinimaController extends AbstractController
{
    /**
     * @Route("/", name="app_estancia_minima_index", methods={"GET"})
     */
    public function index(EstanciaMinimaRepository $estanciaMinimaRepository): Response
    {
        return $this->render('estancia_minima/index.html.twig', [
            'estancia_minimas' => $estanciaMinimaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_estancia_minima_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EstanciaMinimaRepository $estanciaMinimaRepository): Response
    {
        $estanciaMinima = new EstanciaMinima();
        $form = $this->createForm(EstanciaMinimaType::class, $estanciaMinima);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $estanciaMinimaRepository->add($estanciaMinima, true);

            return $this->redirectToRoute('app_estancia_minima_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('estancia_minima/new.html.twig', [
            'estancia_minima' => $estanciaMinima,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_estancia_minima_show", methods={"GET"})
     */
    public function show(EstanciaMinima $estanciaMinima): Response
    {
        return $this->render('estancia_minima/show.html.twig', [
            'estancia_minima' => $estanciaMinima,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_estancia_minima_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, EstanciaMinima $estanciaMinima, EstanciaMinimaRepository $estanciaMinimaRepository): Response
    {
        $form = $this->createForm(EstanciaMinimaType::class, $estanciaMinima);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $estanciaMinimaRepository->add($estanciaMinima, true);

            return $this->redirectToRoute('app_estancia_minima_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('estancia_minima/edit.html.twig', [
            'estancia_minima' => $estanciaMinima,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_estancia_minima_delete", methods={"POST"})
     */
    public function delete(Request $request, EstanciaMinima $estanciaMinima, EstanciaMinimaRepository $estanciaMinimaRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$estanciaMinima->getId(), $request->request->get('_token'))) {
            $estanciaMinimaRepository->remove($estanciaMinima, true);
        }

        return $this->redirectToRoute('app_estancia_minima_index', [], Response::HTTP_SEE_OTHER);
    }
}
