<?php

namespace App\Form;

use App\Entity\AvisosPromociones;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AvisosPromocionesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('hotel')
            ->add('nombre')
            ->add('fecha_ini')
            ->add('fecha_fin')
            ->add('mostrar')
            ->add('terminoscondiciones')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AvisosPromociones::class,
        ]);
    }
}
