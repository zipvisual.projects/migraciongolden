<?php

namespace App\Form;

use App\Entity\TarifaOptional10;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TarifaOptional10Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('hotel')
            ->add('fecha_ini')
            ->add('fecha_fin')
            ->add('adulto')
            ->add('junior')
            ->add('nino')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TarifaOptional10::class,
        ]);
    }
}
