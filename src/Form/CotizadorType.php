<?php

namespace App\Form;

use App\Entity\Cotizador;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CotizadorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('moneda')
            ->add('bonus')
            ->add('fecha')
            ->add('adulto')
            ->add('juniors')
            ->add('ninos')
            ->add('menores')
            ->add('programa')
            ->add('resort')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cotizador::class,
        ]);
    }
}
