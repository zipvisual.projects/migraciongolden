<?php

namespace App\Form;

use App\Entity\TarifaRCIDeposit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TarifaRCIDepositType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('contrato')
            ->add('studio')
            ->add('unobr')
            ->add('dosbr')
            ->add('tresbr')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TarifaRCIDeposit::class,
        ]);
    }
}
