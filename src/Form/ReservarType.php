<?php

namespace App\Form;

use App\Entity\Reservar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tipo_reservacion')
            ->add('idioma_socio')
            ->add('owner_id')
            ->add('numero_contrato')
            ->add('nombre_socio')
            ->add('cert')
            ->add('fecha_llegada')
            ->add('fecha_salida')
            ->add('hotel')
            ->add('habitacion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservar::class,
        ]);
    }
}
