<?php

namespace App\Form;

use App\Entity\TarifaFijoFlotante;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TarifaFijoFlotanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fecha_ini', TextType::class, [
                'attr' => [
                    'class' => 'form-control datetimepicker-input',
                    'data-target' => '#reservationdate',
                    'placeholder' => 'Fecha inicial'
                ]
            ])
            ->add('fecha_fin', TextType::class, [
                'attr' => [
                    'class' => 'form-control datetimepicker-input',
                    'data-target' => '#reservationdate1',
                    'placeholder' => 'Fecha final'
                ]
            ])
            ->add('tarifa', NumberType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Entre la tarifa'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TarifaFijoFlotante::class,
        ]);
    }
}
