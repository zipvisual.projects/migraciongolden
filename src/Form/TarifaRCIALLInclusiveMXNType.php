<?php

namespace App\Form;

use App\Entity\TarifaRCIALLInclusiveMXN;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TarifaRCIALLInclusiveMXNType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('hotel')
            ->add('fecha_ini')
            ->add('fecha_fin')
            ->add('adulto')
            ->add('junior')
            ->add('nino')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TarifaRCIALLInclusiveMXN::class,
        ]);
    }
}
