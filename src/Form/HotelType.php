<?php

namespace App\Form;

use App\Entity\Hotel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HotelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('prefijo_folio')
            ->add('plan')
            ->add('email')
            ->add('descuento_socio')
            ->add('banco')
            ->add('sucursal')
            ->add('beneficiario')
            ->add('numero_cuenta')
            ->add('clabe')
            ->add('adultos')
            ->add('adultos_max')
            ->add('ninos')
            ->add('ninos_max')
            ->add('menores')
            ->add('menores_max')
            ->add('juniors')
            ->add('juniors_max')
            ->add('tarifaFijoFlotante')
            ->add('optionalAI')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Hotel::class,
        ]);
    }
}
