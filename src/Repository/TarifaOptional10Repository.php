<?php

namespace App\Repository;

use App\Entity\TarifaOptional10;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TarifaOptional10>
 *
 * @method TarifaOptional10|null find($id, $lockMode = null, $lockVersion = null)
 * @method TarifaOptional10|null findOneBy(array $criteria, array $orderBy = null)
 * @method TarifaOptional10[]    findAll()
 * @method TarifaOptional10[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaOptional10Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TarifaOptional10::class);
    }

    public function add(TarifaOptional10 $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TarifaOptional10 $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TarifaOptional10[] Returns an array of TarifaOptional10 objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TarifaOptional10
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
