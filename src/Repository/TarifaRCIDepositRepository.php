<?php

namespace App\Repository;

use App\Entity\TarifaRCIDeposit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TarifaRCIDeposit>
 *
 * @method TarifaRCIDeposit|null find($id, $lockMode = null, $lockVersion = null)
 * @method TarifaRCIDeposit|null findOneBy(array $criteria, array $orderBy = null)
 * @method TarifaRCIDeposit[]    findAll()
 * @method TarifaRCIDeposit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaRCIDepositRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TarifaRCIDeposit::class);
    }

    public function add(TarifaRCIDeposit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TarifaRCIDeposit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TarifaRCIDeposit[] Returns an array of TarifaRCIDeposit objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TarifaRCIDeposit
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
