<?php

namespace App\Repository;

use App\Entity\AvisosPromociones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AvisosPromociones>
 *
 * @method AvisosPromociones|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvisosPromociones|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvisosPromociones[]    findAll()
 * @method AvisosPromociones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvisosPromocionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AvisosPromociones::class);
    }

    public function add(AvisosPromociones $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AvisosPromociones $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return AvisosPromociones[] Returns an array of AvisosPromociones objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?AvisosPromociones
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
