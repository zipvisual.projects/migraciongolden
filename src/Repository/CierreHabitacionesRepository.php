<?php

namespace App\Repository;

use App\Entity\CierreHabitaciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CierreHabitaciones>
 *
 * @method CierreHabitaciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method CierreHabitaciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method CierreHabitaciones[]    findAll()
 * @method CierreHabitaciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CierreHabitacionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CierreHabitaciones::class);
    }

    public function add(CierreHabitaciones $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CierreHabitaciones $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return CierreHabitaciones[] Returns an array of CierreHabitaciones objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CierreHabitaciones
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
