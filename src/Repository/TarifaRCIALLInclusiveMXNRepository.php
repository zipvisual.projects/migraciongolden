<?php

namespace App\Repository;

use App\Entity\TarifaRCIALLInclusiveMXN;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TarifaRCIALLInclusiveMXN>
 *
 * @method TarifaRCIALLInclusiveMXN|null find($id, $lockMode = null, $lockVersion = null)
 * @method TarifaRCIALLInclusiveMXN|null findOneBy(array $criteria, array $orderBy = null)
 * @method TarifaRCIALLInclusiveMXN[]    findAll()
 * @method TarifaRCIALLInclusiveMXN[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaRCIALLInclusiveMXNRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TarifaRCIALLInclusiveMXN::class);
    }

    public function add(TarifaRCIALLInclusiveMXN $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TarifaRCIALLInclusiveMXN $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TarifaRCIALLInclusiveMXN[] Returns an array of TarifaRCIALLInclusiveMXN objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TarifaRCIALLInclusiveMXN
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
