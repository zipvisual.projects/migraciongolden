<?php

namespace App\Repository;

use App\Entity\EstanciaMinima;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EstanciaMinima>
 *
 * @method EstanciaMinima|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstanciaMinima|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstanciaMinima[]    findAll()
 * @method EstanciaMinima[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstanciaMinimaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstanciaMinima::class);
    }

    public function add(EstanciaMinima $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(EstanciaMinima $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return EstanciaMinima[] Returns an array of EstanciaMinima objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EstanciaMinima
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
