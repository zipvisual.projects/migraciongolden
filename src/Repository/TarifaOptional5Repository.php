<?php

namespace App\Repository;

use App\Entity\TarifaOptional5;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TarifaOptional5>
 *
 * @method TarifaOptional5|null find($id, $lockMode = null, $lockVersion = null)
 * @method TarifaOptional5|null findOneBy(array $criteria, array $orderBy = null)
 * @method TarifaOptional5[]    findAll()
 * @method TarifaOptional5[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaOptional5Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TarifaOptional5::class);
    }

    public function add(TarifaOptional5 $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TarifaOptional5 $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TarifaOptional5[] Returns an array of TarifaOptional5 objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TarifaOptional5
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
