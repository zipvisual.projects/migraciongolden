<?php

namespace App\Repository;

use App\Entity\TarifaFijoFlotante;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TarifaFijoFlotante>
 *
 * @method TarifaFijoFlotante|null find($id, $lockMode = null, $lockVersion = null)
 * @method TarifaFijoFlotante|null findOneBy(array $criteria, array $orderBy = null)
 * @method TarifaFijoFlotante[]    findAll()
 * @method TarifaFijoFlotante[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaFijoFlotanteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TarifaFijoFlotante::class);
    }

    public function add(TarifaFijoFlotante $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TarifaFijoFlotante $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TarifaFijoFlotante[] Returns an array of TarifaFijoFlotante objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TarifaFijoFlotante
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
