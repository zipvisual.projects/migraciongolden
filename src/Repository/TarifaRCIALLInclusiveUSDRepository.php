<?php

namespace App\Repository;

use App\Entity\TarifaRCIALLInclusiveUSD;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TarifaRCIALLInclusiveUSD>
 *
 * @method TarifaRCIALLInclusiveUSD|null find($id, $lockMode = null, $lockVersion = null)
 * @method TarifaRCIALLInclusiveUSD|null findOneBy(array $criteria, array $orderBy = null)
 * @method TarifaRCIALLInclusiveUSD[]    findAll()
 * @method TarifaRCIALLInclusiveUSD[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaRCIALLInclusiveUSDRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TarifaRCIALLInclusiveUSD::class);
    }

    public function add(TarifaRCIALLInclusiveUSD $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TarifaRCIALLInclusiveUSD $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TarifaRCIALLInclusiveUSD[] Returns an array of TarifaRCIALLInclusiveUSD objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TarifaRCIALLInclusiveUSD
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
