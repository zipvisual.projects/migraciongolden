<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class AppFixtures extends Fixture
{
    private $userPasswordHasherInterface;

    public function __construct (UserPasswordHasherInterface $userPasswordHasherInterface) 
    {
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }
    public function load(ObjectManager $manager): void
    {
         $user = new User();
         $user->setEmail('maykelchapmancardoso@gmail.com');
         $user->setPassword(
            $this->userPasswordHasherInterface->hashPassword(
                $user, "Master2015"
            )
        );
        $user->setUsername('maykel');
        $user->setIsActive(1);
        $user->setName('Maykel');
        $user->setRoles(['ROLE_ADMIN']);
         $manager->persist($user);

        $manager->flush();
    }
}
