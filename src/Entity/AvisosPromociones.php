<?php

namespace App\Entity;

use App\Repository\AvisosPromocionesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AvisosPromocionesRepository::class)
 */
class AvisosPromociones
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hotel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_ini;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_fin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $mostrar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $terminoscondiciones;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHotel(): ?string
    {
        return $this->hotel;
    }

    public function setHotel(string $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaIni(): ?string
    {
        return $this->fecha_ini;
    }

    public function setFechaIni(string $fecha_ini): self
    {
        $this->fecha_ini = $fecha_ini;

        return $this;
    }

    public function getFechaFin(): ?string
    {
        return $this->fecha_fin;
    }

    public function setFechaFin(string $fecha_fin): self
    {
        $this->fecha_fin = $fecha_fin;

        return $this;
    }

    public function isMostrar(): ?bool
    {
        return $this->mostrar;
    }

    public function setMostrar(bool $mostrar): self
    {
        $this->mostrar = $mostrar;

        return $this;
    }

    public function getTerminoscondiciones(): ?string
    {
        return $this->terminoscondiciones;
    }

    public function setTerminoscondiciones(string $terminoscondiciones): self
    {
        $this->terminoscondiciones = $terminoscondiciones;

        return $this;
    }
}
