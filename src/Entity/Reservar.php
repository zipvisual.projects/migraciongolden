<?php

namespace App\Entity;

use App\Repository\ReservarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservarRepository::class)
 */
class Reservar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo_reservacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $idioma_socio;

    /**
     * @ORM\Column(type="integer")
     */
    private $owner_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numero_contrato;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_socio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cert;

    /**
     * @ORM\ManyToOne(targetEntity=Hotel::class, inversedBy="reservars")
     */
    private $hotel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_llegada;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_salida;

    /**
     * @ORM\ManyToMany(targetEntity=Habitacion::class, inversedBy="reservars")
     */
    private $habitacion;

    public function __construct()
    {
        $this->habitacion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipoReservacion(): ?string
    {
        return $this->tipo_reservacion;
    }

    public function setTipoReservacion(string $tipo_reservacion): self
    {
        $this->tipo_reservacion = $tipo_reservacion;

        return $this;
    }

    public function getIdiomaSocio(): ?string
    {
        return $this->idioma_socio;
    }

    public function setIdiomaSocio(string $idioma_socio): self
    {
        $this->idioma_socio = $idioma_socio;

        return $this;
    }

    public function getOwnerId(): ?int
    {
        return $this->owner_id;
    }

    public function setOwnerId(int $owner_id): self
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    public function getNumeroContrato(): ?string
    {
        return $this->numero_contrato;
    }

    public function setNumeroContrato(string $numero_contrato): self
    {
        $this->numero_contrato = $numero_contrato;

        return $this;
    }

    public function getNombreSocio(): ?string
    {
        return $this->nombre_socio;
    }

    public function setNombreSocio(string $nombre_socio): self
    {
        $this->nombre_socio = $nombre_socio;

        return $this;
    }

    public function getCert(): ?string
    {
        return $this->cert;
    }

    public function setCert(string $cert): self
    {
        $this->cert = $cert;

        return $this;
    }

    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function setHotel(?Hotel $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getFechaLlegada(): ?string
    {
        return $this->fecha_llegada;
    }

    public function setFechaLlegada(string $fecha_llegada): self
    {
        $this->fecha_llegada = $fecha_llegada;

        return $this;
    }

    public function getFechaSalida(): ?string
    {
        return $this->fecha_salida;
    }

    public function setFechaSalida(string $fecha_salida): self
    {
        $this->fecha_salida = $fecha_salida;

        return $this;
    }

    /**
     * @return Collection<int, Habitacion>
     */
    public function getHabitacion(): Collection
    {
        return $this->habitacion;
    }

    public function addHabitacion(Habitacion $habitacion): self
    {
        if (!$this->habitacion->contains($habitacion)) {
            $this->habitacion[] = $habitacion;
        }

        return $this;
    }

    public function removeHabitacion(Habitacion $habitacion): self
    {
        $this->habitacion->removeElement($habitacion);

        return $this;
    }
}
