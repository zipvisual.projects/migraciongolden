<?php

namespace App\Entity;

use App\Repository\HotelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HotelRepository::class)
 */
class Hotel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prefijo_folio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $plan;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descuento_socio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $banco;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sucursal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $beneficiario;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numero_cuenta;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clabe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adultos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adultos_max;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ninos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ninos_max;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $menores;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $menores_max;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $juniors;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $juniors_max;

    /**
     * @ORM\ManyToOne(targetEntity=TarifaFijoFlotante::class, inversedBy="hotel")
     */
    private $tarifaFijoFlotante;

    /**
     * @ORM\ManyToOne(targetEntity=OptionalAI::class, inversedBy="hotel")
     */
    private $optionalAI;

    /**
     * @ORM\OneToMany(targetEntity=Reservar::class, mappedBy="hotel")
     */
    private $reservars;

    public function __construct()
    {
        $this->reservars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getPrefijoFolio(): ?string
    {
        return $this->prefijo_folio;
    }

    public function setPrefijoFolio(string $prefijo_folio): self
    {
        $this->prefijo_folio = $prefijo_folio;

        return $this;
    }

    public function getPlan(): ?string
    {
        return $this->plan;
    }

    public function setPlan(string $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescuentoSocio(): ?string
    {
        return $this->descuento_socio;
    }

    public function setDescuentoSocio(string $descuento_socio): self
    {
        $this->descuento_socio = $descuento_socio;

        return $this;
    }

    public function getBanco(): ?string
    {
        return $this->banco;
    }

    public function setBanco(string $banco): self
    {
        $this->banco = $banco;

        return $this;
    }

    public function getSucursal(): ?string
    {
        return $this->sucursal;
    }

    public function setSucursal(string $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    public function getBeneficiario(): ?string
    {
        return $this->beneficiario;
    }

    public function setBeneficiario(string $beneficiario): self
    {
        $this->beneficiario = $beneficiario;

        return $this;
    }

    public function getNumeroCuenta(): ?string
    {
        return $this->numero_cuenta;
    }

    public function setNumeroCuenta(string $numero_cuenta): self
    {
        $this->numero_cuenta = $numero_cuenta;

        return $this;
    }

    public function getClabe(): ?string
    {
        return $this->clabe;
    }

    public function setClabe(string $clabe): self
    {
        $this->clabe = $clabe;

        return $this;
    }

    public function getAdultos(): ?string
    {
        return $this->adultos;
    }

    public function setAdultos(string $adultos): self
    {
        $this->adultos = $adultos;

        return $this;
    }

    public function getAdultosMax(): ?string
    {
        return $this->adultos_max;
    }

    public function setAdultosMax(string $adultos_max): self
    {
        $this->adultos_max = $adultos_max;

        return $this;
    }

    public function getNinos(): ?string
    {
        return $this->ninos;
    }

    public function setNinos(string $ninos): self
    {
        $this->ninos = $ninos;

        return $this;
    }

    public function getNinosMax(): ?string
    {
        return $this->ninos_max;
    }

    public function setNinosMax(string $ninos_max): self
    {
        $this->ninos_max = $ninos_max;

        return $this;
    }

    public function getMenores(): ?string
    {
        return $this->menores;
    }

    public function setMenores(string $menores): self
    {
        $this->menores = $menores;

        return $this;
    }

    public function getMenoresMax(): ?string
    {
        return $this->menores_max;
    }

    public function setMenoresMax(string $menores_max): self
    {
        $this->menores_max = $menores_max;

        return $this;
    }

    public function getJuniors(): ?string
    {
        return $this->juniors;
    }

    public function setJuniors(string $juniors): self
    {
        $this->juniors = $juniors;

        return $this;
    }

    public function getJuniorsMax(): ?string
    {
        return $this->juniors_max;
    }

    public function setJuniorsMax(string $juniors_max): self
    {
        $this->juniors_max = $juniors_max;

        return $this;
    }

    public function getTarifaFijoFlotante(): ?TarifaFijoFlotante
    {
        return $this->tarifaFijoFlotante;
    }

    public function setTarifaFijoFlotante(?TarifaFijoFlotante $tarifaFijoFlotante): self
    {
        $this->tarifaFijoFlotante = $tarifaFijoFlotante;

        return $this;
    }

    public function getOptionalAI(): ?OptionalAI
    {
        return $this->optionalAI;
    }

    public function setOptionalAI(?OptionalAI $optionalAI): self
    {
        $this->optionalAI = $optionalAI;

        return $this;
    }

    /**
     * @return Collection<int, Reservar>
     */
    public function getReservars(): Collection
    {
        return $this->reservars;
    }

    public function addReservar(Reservar $reservar): self
    {
        if (!$this->reservars->contains($reservar)) {
            $this->reservars[] = $reservar;
            $reservar->setHotel($this);
        }

        return $this;
    }

    public function removeReservar(Reservar $reservar): self
    {
        if ($this->reservars->removeElement($reservar)) {
            // set the owning side to null (unless already changed)
            if ($reservar->getHotel() === $this) {
                $reservar->setHotel(null);
            }
        }

        return $this;
    }
}
