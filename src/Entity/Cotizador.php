<?php

namespace App\Entity;

use App\Repository\CotizadorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CotizadorRepository::class)
 */
class Cotizador
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $moneda;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $bonus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adulto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $juniors;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ninos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $menores;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $programa;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $resort;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getBonus(): ?string
    {
        return $this->bonus;
    }

    public function setBonus(string $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getAdulto(): ?string
    {
        return $this->adulto;
    }

    public function setAdulto(string $adulto): self
    {
        $this->adulto = $adulto;

        return $this;
    }

    public function getJuniors(): ?string
    {
        return $this->juniors;
    }

    public function setJuniors(string $juniors): self
    {
        $this->juniors = $juniors;

        return $this;
    }

    public function getNinos(): ?string
    {
        return $this->ninos;
    }

    public function setNinos(string $ninos): self
    {
        $this->ninos = $ninos;

        return $this;
    }

    public function getMenores(): ?string
    {
        return $this->menores;
    }

    public function setMenores(string $menores): self
    {
        $this->menores = $menores;

        return $this;
    }

    public function getPrograma(): ?string
    {
        return $this->programa;
    }

    public function setPrograma(string $programa): self
    {
        $this->programa = $programa;

        return $this;
    }

    public function getResort(): ?string
    {
        return $this->resort;
    }

    public function setResort(string $resort): self
    {
        $this->resort = $resort;

        return $this;
    }
}
