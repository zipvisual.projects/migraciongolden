<?php

namespace App\Entity;

use App\Repository\OptionalAIRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OptionalAIRepository::class)
 */
class OptionalAI
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Hotel::class, mappedBy="optionalAI")
     */
    private $hotel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_ini;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_fin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adulto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $junior;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nino;

    public function __construct()
    {
        $this->hotel = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Hotel>
     */
    public function getHotel(): Collection
    {
        return $this->hotel;
    }

    public function addHotel(Hotel $hotel): self
    {
        if (!$this->hotel->contains($hotel)) {
            $this->hotel[] = $hotel;
            $hotel->setOptionalAI($this);
        }

        return $this;
    }

    public function removeHotel(Hotel $hotel): self
    {
        if ($this->hotel->removeElement($hotel)) {
            // set the owning side to null (unless already changed)
            if ($hotel->getOptionalAI() === $this) {
                $hotel->setOptionalAI(null);
            }
        }

        return $this;
    }

    public function getFechaIni(): ?string
    {
        return $this->fecha_ini;
    }

    public function setFechaIni(string $fecha_ini): self
    {
        $this->fecha_ini = $fecha_ini;

        return $this;
    }

    public function getFechaFin(): ?string
    {
        return $this->fecha_fin;
    }

    public function setFechaFin(string $fecha_fin): self
    {
        $this->fecha_fin = $fecha_fin;

        return $this;
    }

    public function getAdulto(): ?string
    {
        return $this->adulto;
    }

    public function setAdulto(string $adulto): self
    {
        $this->adulto = $adulto;

        return $this;
    }

    public function getJunior(): ?string
    {
        return $this->junior;
    }

    public function setJunior(string $junior): self
    {
        $this->junior = $junior;

        return $this;
    }

    public function getNino(): ?string
    {
        return $this->nino;
    }

    public function setNino(string $nino): self
    {
        $this->nino = $nino;

        return $this;
    }
}
