<?php

namespace App\Entity;

use App\Repository\TarifaRCIDepositRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifaRCIDepositRepository::class)
 */
class TarifaRCIDeposit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contrato;

    /**
     * @ORM\Column(type="integer")
     */
    private $studio;

    /**
     * @ORM\Column(type="integer")
     */
    private $unobr;

    /**
     * @ORM\Column(type="integer")
     */
    private $dosbr;

    /**
     * @ORM\Column(type="integer")
     */
    private $tresbr;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContrato(): ?string
    {
        return $this->contrato;
    }

    public function setContrato(string $contrato): self
    {
        $this->contrato = $contrato;

        return $this;
    }

    public function getStudio(): ?int
    {
        return $this->studio;
    }

    public function setStudio(int $studio): self
    {
        $this->studio = $studio;

        return $this;
    }

    public function getUnobr(): ?int
    {
        return $this->unobr;
    }

    public function setUnobr(int $unobr): self
    {
        $this->unobr = $unobr;

        return $this;
    }

    public function getDosbr(): ?int
    {
        return $this->dosbr;
    }

    public function setDosbr(int $dosbr): self
    {
        $this->dosbr = $dosbr;

        return $this;
    }

    public function getTresbr(): ?int
    {
        return $this->tresbr;
    }

    public function setTresbr(int $tresbr): self
    {
        $this->tresbr = $tresbr;

        return $this;
    }
}
