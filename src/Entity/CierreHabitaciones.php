<?php

namespace App\Entity;

use App\Repository\CierreHabitacionesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CierreHabitacionesRepository::class)
 */
class CierreHabitaciones
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hotel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $habitacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_ini;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_fin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cerrado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHotel(): ?string
    {
        return $this->hotel;
    }

    public function setHotel(string $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getHabitacion(): ?string
    {
        return $this->habitacion;
    }

    public function setHabitacion(string $habitacion): self
    {
        $this->habitacion = $habitacion;

        return $this;
    }

    public function getFechaIni(): ?string
    {
        return $this->fecha_ini;
    }

    public function setFechaIni(string $fecha_ini): self
    {
        $this->fecha_ini = $fecha_ini;

        return $this;
    }

    public function getFechaFin(): ?string
    {
        return $this->fecha_fin;
    }

    public function setFechaFin(string $fecha_fin): self
    {
        $this->fecha_fin = $fecha_fin;

        return $this;
    }

    public function isCerrado(): ?bool
    {
        return $this->cerrado;
    }

    public function setCerrado(bool $cerrado): self
    {
        $this->cerrado = $cerrado;

        return $this;
    }
}
