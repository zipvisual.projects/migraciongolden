<?php

namespace App\Entity;

use App\Repository\HabitacionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HabitacionRepository::class)
 */
class Habitacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity=TarifaFijoFlotante::class, inversedBy="habitacion")
     */
    private $tarifaFijoFlotante;

    /**
     * @ORM\ManyToMany(targetEntity=Reservar::class, mappedBy="habitacion")
     */
    private $reservars;

    public function __construct()
    {
        $this->reservars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTarifaFijoFlotante(): ?TarifaFijoFlotante
    {
        return $this->tarifaFijoFlotante;
    }

    public function setTarifaFijoFlotante(?TarifaFijoFlotante $tarifaFijoFlotante): self
    {
        $this->tarifaFijoFlotante = $tarifaFijoFlotante;

        return $this;
    }

    /**
     * @return Collection<int, Reservar>
     */
    public function getReservars(): Collection
    {
        return $this->reservars;
    }

    public function addReservar(Reservar $reservar): self
    {
        if (!$this->reservars->contains($reservar)) {
            $this->reservars[] = $reservar;
            $reservar->addHabitacion($this);
        }

        return $this;
    }

    public function removeReservar(Reservar $reservar): self
    {
        if ($this->reservars->removeElement($reservar)) {
            $reservar->removeHabitacion($this);
        }

        return $this;
    }
}
