<?php

namespace App\Entity;

use App\Repository\TarifaOptional5Repository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifaOptional5Repository::class)
 */
class TarifaOptional5
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hotel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_ini;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_fin;

    /**
     * @ORM\Column(type="integer")
     */
    private $adulto;

    /**
     * @ORM\Column(type="integer")
     */
    private $junior;

    /**
     * @ORM\Column(type="integer")
     */
    private $nino;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHotel(): ?string
    {
        return $this->hotel;
    }

    public function setHotel(string $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getFechaIni(): ?string
    {
        return $this->fecha_ini;
    }

    public function setFechaIni(string $fecha_ini): self
    {
        $this->fecha_ini = $fecha_ini;

        return $this;
    }

    public function getFechaFin(): ?string
    {
        return $this->fecha_fin;
    }

    public function setFechaFin(string $fecha_fin): self
    {
        $this->fecha_fin = $fecha_fin;

        return $this;
    }

    public function getAdulto(): ?int
    {
        return $this->adulto;
    }

    public function setAdulto(int $adulto): self
    {
        $this->adulto = $adulto;

        return $this;
    }

    public function getJunior(): ?int
    {
        return $this->junior;
    }

    public function setJunior(int $junior): self
    {
        $this->junior = $junior;

        return $this;
    }

    public function getNino(): ?int
    {
        return $this->nino;
    }

    public function setNino(int $nino): self
    {
        $this->nino = $nino;

        return $this;
    }
}
