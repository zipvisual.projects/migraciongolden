<?php

namespace App\Entity;

use App\Repository\TarifaFijoFlotanteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifaFijoFlotanteRepository::class)
 */
class TarifaFijoFlotante
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Hotel::class, mappedBy="tarifaFijoFlotante")
     */
    private $hotel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_ini;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_fin;

    /**
     * @ORM\OneToMany(targetEntity=Habitacion::class, mappedBy="tarifaFijoFlotante")
     */
    private $habitacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tarifa;

    public function __construct()
    {
        $this->hotel = new ArrayCollection();
        $this->habitacion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Hotel>
     */
    public function getHotel(): Collection
    {
        return $this->hotel;
    }

    public function addHotel(Hotel $hotel): self
    {
        if (!$this->hotel->contains($hotel)) {
            $this->hotel[] = $hotel;
            $hotel->setTarifaFijoFlotante($this);
        }

        return $this;
    }

    public function removeHotel(Hotel $hotel): self
    {
        if ($this->hotel->removeElement($hotel)) {
            // set the owning side to null (unless already changed)
            if ($hotel->getTarifaFijoFlotante() === $this) {
                $hotel->setTarifaFijoFlotante(null);
            }
        }

        return $this;
    }

    public function getFechaIni(): ?string
    {
        return $this->fecha_ini;
    }

    public function setFechaIni(string $fecha_ini): self
    {
        $this->fecha_ini = $fecha_ini;

        return $this;
    }

    public function getFechaFin(): ?string
    {
        return $this->fecha_fin;
    }

    public function setFechaFin(string $fecha_fin): self
    {
        $this->fecha_fin = $fecha_fin;

        return $this;
    }

    /**
     * @return Collection<int, Habitacion>
     */
    public function getHabitacion(): Collection
    {
        return $this->habitacion;
    }

    public function addHabitacion(Habitacion $habitacion): self
    {
        if (!$this->habitacion->contains($habitacion)) {
            $this->habitacion[] = $habitacion;
            $habitacion->setTarifaFijoFlotante($this);
        }

        return $this;
    }

    public function removeHabitacion(Habitacion $habitacion): self
    {
        if ($this->habitacion->removeElement($habitacion)) {
            // set the owning side to null (unless already changed)
            if ($habitacion->getTarifaFijoFlotante() === $this) {
                $habitacion->setTarifaFijoFlotante(null);
            }
        }

        return $this;
    }

    public function getTarifa(): ?string
    {
        return $this->tarifa;
    }

    public function setTarifa(string $tarifa): self
    {
        $this->tarifa = $tarifa;

        return $this;
    }
}
